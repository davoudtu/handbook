---
title: Enterprise Applications
---
## CRM Systems, Director

The CRM Systems, Director reports to the Senior Director, Enterprise Applications, and leads a team of highly-collaborative and results-oriented IT staff tasked with delivering global, high value IT applications across the company. The Director has the skills, experience, drive and passion to ensure that GitLab has the appropriate IT applications to achieve/exceed corporate objectives; to ensure business alignment with team members and sponsors; and to anticipate the needs of the business and scale the team to stay ahead of the curve. This position has deep technical skills in Salesforce and can drive the roadmap across the Sales Systems tech stack including such systems as Billing systems (Zuora, NetSuite), Marketing systems (Marketo), ZenDesk/Service Cloud/ServiceNow, Sales Enablement tools (WalkMe, SpekIt), CLM, and Integrations. This function challenges the status quo; thrives and is successful in a dynamic environment. This team member must be motivated and successful at mentoring by building and sustaining professional relationships at all levels of an organization. The Director has well-honed problem solving and decision making skills, is agile and adaptable, and can effectively manage change. This function is both strategic and tactical, can influence adoption, is results oriented, and has a strong client service orientation. The Director has the reputation for raising the bar in terms of quality and execution.

### Job Grade

CRM Systems, Director is a [grade 10](https://handbook.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) and reports to the [Director, Enterprise Applications](https://handbook.gitlab.com/job-families/finance/senior-director-enterprise-applications/)

### Responsibilities

- Business Partnership: Influences leadership within the cross-functional team and assists in roadmap planning. Solid relationships with key stakeholders at the VP level.
- Business Operations: Analyze and plan improvements in technical implementations to continue to deliver value to business partners. Drive the business to adopt recommended technical solutions
- Document related processes and technical configurations in the handbook.
- Technical Delivery: Provide hands-on, expert-level technical assistance to delivery teams. Drives adoption of technology and process changes. Drive end to end delivery on large, complex, cross-functional projects across multiple systems or integrations with minimal oversight.
- Proactively provides recommendations for system improvements.
- Project Management: Run the technical implementation of large-sized projects which improve our ability to make better data driven insights, or make the company more efficient. Drive intake and prioritization process for functional areas of expertise.
- Communication: Target audience Director, Sr. Director, VP, with presentations targeted to CFO staff for critical projects.
Mentorship: Build and manage global teams. Guide and coach team members. Mentors junior team members and models GitLab values for the team.
- Measure, monitor, and maintain team’s ability to meet or exceed Service Level Agreements (SLA), Objectives and Key Results (OKRs), and Key Performance Indicators (KPIs). Experience and comfort working in a culture of measurement


### Requirements

- Bachelor’s degree, preferably in Computer Science, Information Technology, Computer Engineering, or related IT discipline; or equivalent experience. 10-15 years of experience implementing, configuring, customizing and integrating SaaS software Eg: SalesForce, DocuSign CLM, Netsuite, Zuora Billing, Zuora Revenue, Coupa, Zip, Navan, Workday, ADP, eTrade etc.
- 10-15 years’ experience in IT, with 7+ years’ experience leading and managing an IT organization or Applications group at a fast growing high technology company.
- Be a self-starter, and thrive in a fast paced environment, customer centric and have the interpersonal skills necessary to manage business and technology relationships.
- Exceptional time management and prioritization skills, resilient under pressure, able to meet deadlines and able to work in a constantly changing environment.
- Exhibit strong security and confidentiality practices with systems that handle sensitive data. Experience partnering with internal information security and compliance teams. SOX experience is preferred
- Strong understanding of GTM and LTC business processes in a Salesforce environment.
- Industry certifications a plus: Salesforce, PMP, ITIL, CISSP or similar certification
- Proven ability to effectively lead and meet business objectives in a global, collaborative and high performance work environment
- Change management knowledge and ability to operate effectively in fast paced environment
- Demonstrated experience in vendor management and capacity planning in a fast growth environment
- Nice to have, application specific skills:
    - Salesforce - Development experience with advanced Salesforce.com development tools and techniques (e.g. Apex language, Force.com custom development, triggers, controllers, JavaScript, Force.com SOAP and REST APIs, JSON, AngularJS, jQuery, JavaScript, sObjects, SOQL, SOSL, Chatter APIs, outbound messaging, Bootstrap, Lightning Design System (LDS), Lightning Components and Visualforce pages).
    - Marketo
    - GainSight
    - ZenDesk / Service Cloud / ServiceNow
    - Enablement tools, such as WalkMe, SpekIt, etc.
    - CLM: DocuSign CLM, Ironclad
    - Billing Systems: CPQ, Billing, Revenue (Zuora, NetSuite, etc.)
    - Integration: Mulesoft, Talend, Workato etc.


## Senior Director, Enterprise Applications

The Senior Director, Enterprise Applications, reports to the VP of Information Technology, and leads a team of highly-collaborative and results-oriented IT staff tasked with delivering global, high value IT applications across the company. The Senior Director has the skills, experience, drive and passion to ensure that GitLab has the appropriate IT applications to achieve/exceed corporate objectives; to ensure business alignment with executive team members and sponsors; and to anticipate the needs of the business and scale the team to stay ahead of the curve. This function challenges the status quo; thrives and is successful in a dynamic environment.
This team member must be motivated and successful at mentoring by building and sustaining professional relationships at all levels of an organization. The Senior Director has well-honed problem solving and decision making skills, is agile and adaptable, and can effectively manage change. This function is both strategic and tactical, can influence adoption, is results oriented, and has a strong client service orientation. The Senior Director has the reputation for raising the bar in terms of quality and execution.

### Job Grade

The Senior Director, Enterprise Applications is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Strategic and operational IT Application ownership for the following areas: Finance, HR, Sales, Marketing, Support, and Legal
- Drive executive level communication / support for the IT Applications strategy
- Partner with key business sponsors / executive stakeholders to deliver high value IT solutions
- Drive operational excellence while leveraging KPI's to measure the success/value of the IT Applications team
- Develop and maintain relationships with client organizations to ensure their IT Application expectations are being met and priorities are set at the appropriate level.
- Develop and manage plan of record for the IT Applications portfolio; while driving culture of delivery to commitments

### Requirements

- Fifteen+ years’ experience in IT, with ten+ years' experience leading and managing an IT organization or Applications group at a fast growing high technology company.
- Excellent change management skills and ability to operate effectively in a fast-paced environment.
- Experience with SaaS based solution and cloud architectures.
- Demonstrated ability to successfully lead and develop global applications organization in collaboration with overall IT strategy.
- Demonstrated strength in contract negotiations, vendor management, and capacity planning in a fast growth environment.
- Knowledgeable in compliance and information security.
- Current on business applications, trends in IT and experience evaluating/implementing new innovative IT solutions
- A process-oriented individual, with substantial project management skills, who has the flexibility to thrive in a fast-paced, dynamic organization.
- Candidate must have proven ability to consistently and collaboratively resolve issues, mitigate roadblocks, and meet all financial and management goals on time. Will be a self-starter who maps own direction to succeed.
- Excellent team management, coaching, and mentoring skills
- Must be comfortable with ambiguity and fast change with an ability to adapt quickly and easily.
- Relevant business/industry acumen with the ability to quickly and thoroughly understand business priorities, operations, and IT enablement potential.
- Ability to use GitLab

## Performance Indicators (PI)

- [New Hire Location Factor < 0.69](/handbook/business-technology/metrics/#new-hire-location-factor--069)
- [% of team who self-classify as diverse](/handbook/business-technology/metrics/#percent--of-team-who-self-classify-as-diverse)
- [Discretionary bonus per employee per month > 0.1](/handbook/business-technology/metrics/#discretionary-bonus-per-employee-per-month--01)
- [Cost Actual vs Plan](/handbook/business-technology/metrics/#cost-actual-vs-plan)
- [System roll out vs plan](/handbook/business-technology/metrics/#system-roll-out-vs-plan)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/handbook/company/team/).

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with our VP, Information Technology
- Candidates will then be invited to schedule a second interview with our CFO, VP, Field Operations, and VP, Finance
- Finally, candidates may be asked to interview with our CEO

## Career Ladder

The next step in the Enterprise Applications job family is to move to the [VP of IT](/job-families/finance/vp-information-technology) job family.
