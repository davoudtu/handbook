---
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter   | Candidate Experience Specialist    |
|--------------------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn    | Michelle Jubrey  |
| Executive          | Zach Choquette   | Michelle Jubrey  |
| Enterprise Sales, AMER | Kevin Rodrigues |Fernando Khubeir |
| Enterprise Sales, EMEA | Kannwal Matharu | Lerato Thipe |
| Sales, AMER | Marcus Carter | Michelle Jubrey |
| Sales/ G&A | Hannah Stewart  | Fernando Khubeir |
| Commercial Sales/R&D, EMEA | Ben Cowdry | Lerato Thipe |
| Enterprise Sales, EMEA | Kanwal Matharu  | Lerato Thipe |
| Sales | Kelsey Hart  | Fernando Khubeir |
| Customer Success, EMEA | Joanna Tourne | Lerato Thipe |
| Customer Success, AMER | Barbara Dinoff |  Fernando Khubeir |
| All Business, APAC | Yas Priatna  | Lerato Thipe |
| Marketing/ G&A, Global | Steph Sarff | Michelle Jubrey |
| Marketing, BDR/SDR/G&A (Global)| Caroline Rebello |  Alice Crosbie |
| G&A | Jenna VanZutphen | Fernando Khubeir |
| Development | Mark Deubel & Sara Currie | Alice Crosbie |
 R&D | Mark Deubel | Alice Crosbie |
| R&D   | Michelle A. Kemp | Alice Crosbie  |
| R&D | Joanna Michniewicz  |  Alice Crosbie |
| R&D | Holly Nesselroad | Michelle Jubrey |
| Product Management  | Holly Nesselroad | Michelle Jubrey |
| R&D  | Riley Smith | Lerato Thipe  |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails.

## Talent Acquisition Leader Alignment

| Department                    | Leader      |
|--------------------------|-----------------|
| Talent Acquisition         | Jess Dallmar |
| Talent Brand | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (EMEA and APAC Sales) | Jake Foster |
| Talent Acquisition (Marketing) | Steph Sarff + Jake Foster |
| Talent Acquisition (G&A) | Steph Sarff + Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Talent Acquisition (R&D: Customer Support & Development) | Ursela Knezevic |
| Talent Acquisition (R&D: Infrastructure/Quality, Security, Product/UX, Incubation) | Ursela Knezevic |
| Talent Acquisition (Executive) | Rich Kahn |
| Enablement | Marissa Ferber |
| Candidate Experience | Ale Ayala/Marissa Ferber |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
